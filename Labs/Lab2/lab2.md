# Lab 2: Git

1. Practice using the [Git Simulator](./git-sim)

2. Try [merging branches](./git-conflict) and conflict resolution

3. Learn how to create [your own repo](./git-pr)
